const Users = require('../models/register.model.js');

exports.regUser = (req, res) => {
    const register = new Users({
        name: req.body.name,
        email: req.body.email,
        passwd: req.body.password,
        dob: req.body.dob,
        location: req.body.location,
        phone: req.body.phone,
        deleted: 'N'
    })
    register.save()
    .then(data => {
        res.send("User Registered");
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occured while registering User"
        });
    });
};


exports.getAllUser = (req, res) => {
    Users.find({},{_id:false,name:true, email:true, phone:true})
    .then(data => {
        var element = [];
        for (let val = 0; val < data.length; val++) {
            element.push(data[val]);
        }
        res.json(element);
        
    }).catch( err => {
        res.status(500).send({
            message: err.message || "Error while retriving data"
        });
    });
};

