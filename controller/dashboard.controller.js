const Users = require('../models/register.model.js');

exports.listUser = (req, res) => {

    Users.find({deleted: 'N'},{name:true, email:true, phone:true})
    .then(data => {
        var element = [];
        for (let val = 0; val < data.length; val++) {
            element.push(data[val]);
        }
        res.render('Users', { dashData: element });
        
    }).catch( err => {
        res.status(500).send({
            message: err.message || "Error while retriving data"
        });
    });

};
// Edit User Controller

exports.userProfile = (req, res) => {
    Users.findById(req.params.ouid)
    .then(data => {
        res.render('profile', {profileData: data});
    }).catch(err => {
        return res.status(404).send({
            message: "User not found"
        });
    });
};

//  Profile Update
exports.updateUserProfile = (req, res) => {
    Users.findByIdAndUpdate(req.params.ouid, {
        name: req.body.name,
        location: req.body.location,
        phone: req.body.phone
    }, {new: true})
    .then(data => {
        if (!data) {
            return res.status(404).send({
                message: "Data not found in database"
            });
        }
        res.send({message: 'User details updated!'});
    }).catch(err => {
        return res.send(500).send({
            message: "Error while updating data"
        });
    });
};

// Detete User Controller
exports.deleteUser = (req, res) => {
    Users.findByIdAndUpdate(req.params.ouid, {
        deleted: 'Y'
    }, {new: true})
    .then(data => {
        if (!data) {
            return res.status(404).send({
                message: "User not found in Database"
            });
        }
        res.send({message: "User Deleted from Database"});
    }).catch(err => {
        return res.status(500).send({
            message: "Could not Delete the user"
        });
    });
};

