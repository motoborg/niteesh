var express = require('express');
var router = express.Router();

const dashController = require('../controller/dashboard.controller.js');

/* GET users listing. */
router.get('/', dashController.listUser);

router.get('/delete/:ouid', dashController.deleteUser);

// profile route

router.get('/profile/:ouid', dashController.userProfile);

// Handle update changes
router.post('/profile/:ouid', dashController.updateUserProfile);


module.exports = router;
