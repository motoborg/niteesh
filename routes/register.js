var express = require('express');
var router = express.Router();

const regController = require('../controller/register.controller.js');

router.get('/', function (req, res, next) {
    res.render('register' , { title : 'Register Account'});
});

router.post('/', function (req, res, next) {
    regController.regUser(req, res) 
})

module.exports = router;