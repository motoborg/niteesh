const mongoose = require('mongoose');

const regSchema = mongoose.Schema({
    name: String,
    email: String,
    passwd: String,
    dob: Date,
    location: String,
    phone: Number,
    deleted: String
})

module.exports = mongoose.model('users', regSchema);